package com.xminds.basics;
import java.io.Console;
import java.util.Scanner;

public class FirstProgram {

    public static void main(String[] args)
    {
        System.out.println("Hello World !!");

            //1.Write a program to calculate the sum of first 10 natural number.
            sumOfFirstTenNumbers();
        try {
            //2.Write a program to find the factorial value of any number entered through the keyboard?
            factorialOfNumber();
        }
        catch(Exception e)
        {
            System.out.println("Something went wrong ");
        }
        /* 3. Two numbers are entered through the keyboard.
        Write a program to find the value of one number raised to the power of another. (Do not use Java built-in method)
        */
        try {
            power();

    }
        catch(Exception e)
    {
        System.out.println("Something went wrong ");
    }
        /*
        4.Write a program that prompts the user to input an integer and then outputs the number with the digits reversed.
        For example, if the input is 12345, the output should be 54321.
        */
            digitsReversed();
            //5.Write a program that reads a set of integers, and then prints the sum of the even and odd integers?
            sumOfEvenAndOddIntegers();
            //6.Write a program that prompts the user to input a positive integer. It should then output a message indicating whether the number is a prime number.
            CheckPrime();
            //7.Write a program to enter the numbers till the user wants and at the end it should display the count of positive, negative and zeros entered.?


    }

    private static void  CheckPrime() {
        int temp;
        boolean isPrime=true;

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter any positive number:");

        int num=scan.nextInt();

        scan.close();
        if(num > 0)
        {
            for(int i=2;i<=num/2;i++)
            {
                temp=num%i;
                if(temp==0)
                {
                    isPrime=false;
                    break;
                }
            }

            if(isPrime)
                System.out.println(num + " is a Prime Number");
            else
                System.out.println(num + " is not a Prime Number");
        }
        else if(num==0)
        {
            System.out.println(num+" is neither positive nor negative.Please enter a positive number");
        }
        else
        {
            System.out.println(num+" is negative number.Please enter a positive number");
        }


    }
    private static void sumOfEvenAndOddIntegers() {

        
        int even = 0;
        int odd = 0;
        char choice;

        do
        {
            Scanner scan= new Scanner(System.in);
            System.out.println("Enter any positive number:");

            int number=scan.nextInt();

            if( number % 2 == 0)
            {
                even += number;
            }
            else
            {
                odd += number;
            }

            System.out.print("Do you want to continue y/n? ");

            choice = scan.next().charAt(0);

        }while(choice=='y' || choice == 'Y');

        System.out.println("Sum of even numbers: " + even);
        System.out.println("Sum of odd numbers: " + odd);
    }
    private static void digitsReversed() {


        int reversenum = 0,num =0;

        System.out.println("Input your number and press enter: ");


        Scanner in = new Scanner(System.in);
        num = in.nextInt();
        while( num != 0 )

        {

            reversenum = reversenum * 10;

            reversenum = reversenum + num%10;

            num = num/10;

        }

        System.out.println("Reverse of input number is: "+reversenum);
    }
    private static void  power() {

        Scanner sc = new Scanner(System.in);

        System.out.println("ENTER THE NO. ");

        int n1 = sc.nextInt();

        System.out.println("ENTER THE POWER FOR THAT NO.");

        int n2 = sc.nextInt();

        int power = 1;

        if (n2 >= 1) {

            for (int i = 1; i <= n2; i++) {

                power = power * n1;

            }

            System.out.println(power);
        }


    }
    private static void factorialOfNumber() {
        int i,fact=1;
        Scanner console = new Scanner(System.in);
        System.out.print("Enter any positive integer: ");
        int number=console.nextInt();

        for(i=1;i<=number;i++){
            fact=fact*i;
        }
        System.out.println("Factorial of "+number+" is: "+fact);
    }
    private static void sumOfFirstTenNumbers() {
        int i, num = 10, sum = 0;

        for(i = 1; i <= num; ++i)
        {
           sum = sum + i;
        }
        System.out.println("Sum of First 10 Natural Numbers is = " + sum);
    }

}
